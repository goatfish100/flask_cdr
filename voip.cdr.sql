-- Table: voip.cdr

-- DROP TABLE voip.cdr;
CREATE SEQUENCE voip.cdr_id_seq;

CREATE TABLE voip.cdr
(
    id bigint NOT NULL DEFAULT nextval('voip.cdr_id_seq'::regclass),
    local_ip_v4 inet NOT NULL,
    caller_id_name character varying COLLATE pg_catalog."default",
    caller_id_number character varying COLLATE pg_catalog."default",
    destination_number character varying COLLATE pg_catalog."default" NOT NULL,
    context character varying COLLATE pg_catalog."default" NOT NULL,
    start_stamp timestamp(0) with time zone NOT NULL,
    answer_stamp timestamp(0) with time zone,
    end_stamp timestamp(0) with time zone NOT NULL,
    duration integer NOT NULL,
    billsec integer NOT NULL,
    hangup_cause character varying COLLATE pg_catalog."default" NOT NULL,
    uuid uuid NOT NULL,
    bleg_uuid uuid,
    customer_id integer,
    read_codec character varying COLLATE pg_catalog."default",
    write_codec character varying COLLATE pg_catalog."default",
    sip_hangup_disposition character varying COLLATE pg_catalog."default",
    ani character varying COLLATE pg_catalog."default",
    direction character varying COLLATE pg_catalog."default",
    lrn character varying COLLATE pg_catalog."default",
    rate numeric(7,5),
    total numeric(7,5),
    processed boolean NOT NULL DEFAULT false,
    peer_ip inet,
    destination_ip inet,
    feature jsonb,
    CONSTRAINT cdr_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

-- ALTER TABLE voip.cdr
--     OWNER to gaving;

-- GRANT ALL ON TABLE voip.cdr TO gaving;

-- GRANT ALL ON TABLE voip.cdr TO voip;

-- -- Index: cdrorig

-- -- DROP INDEX voip.cdrorig;

-- CREATE INDEX cdrorig
--     ON voip.cdr USING btree
--     (end_stamp, customer_id)
--     TABLESPACE pg_default;

-- -- Trigger: process_orig_cdr

-- -- DROP TRIGGER process_orig_cdr ON voip.cdr;

-- CREATE TRIGGER process_orig_cdr
--     BEFORE INSERT
--     ON voip.cdr
--     FOR EACH ROW
--     EXECUTE PROCEDURE voip.orig_rate_calc();
