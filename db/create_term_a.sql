DROP TABLE IF EXISTS cdr2.term_a_201811; 
CREATE TABLE cdr2.term_a_201811
(
  uuid uuid,
  call_id character varying,
  peer_ip inet,
  local_ip_v4 character varying,
  caller_id_name character varying,
  caller_id_number character varying,
  ani character varying,
  lrn character varying,
  destination_number character varying,
  direction character varying,
  context character varying,
  start_stamp timestamp(0) with time zone,
  progress_stamp timestamp(0) with time zone,
  answer_stamp timestamp(0) with time zone,
  end_stamp timestamp(0) with time zone,
  cdr_date date,
  duration integer NOT NULL,
  progressmsec integer,
  billsec integer,
  hangup_cause character varying,
  customer_id integer,
  ep_codec_string character varying,
  dtmf_type character varying,
  remote_media_ip inet,
  remote_media_port integer,
  proto_specific_hangup_cause character varying(64),
  originate_disposition character varying,
  sip_term_status character varying,
  sip_term_cause integer,
  hangup_cause_q850 integer,
  digits_dialed character varying,
  sip_hangup_disposition character varying,
  rtp_quality numeric(5,2),
  rtp_mos numeric(2,1),
  rtp_audio_out_packet_count integer,
  rtp_audio_in_packet_count integer,
  rate numeric(7,5),
  total numeric(8,5),
  processed boolean NOT NULL DEFAULT false,
  jurisdiction character varying(32),
  state character varying(32),
  proxy_ip inet,
  cdrtag character varying(128)
)
WITH (
  OIDS = FALSE
);

