import logging
import json
import os
from os.path import join, dirname
from dotenv import load_dotenv
 
#load env variables from .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
HTTP_PORT = os.getenv('HTTP_PORT')

from flask import Flask
from flask import request
app = Flask(__name__)
 
@app.route("/hello")
def hello():
    return "Hello World!"

@app.route("/postjson", methods = ['POST'])
def postJsonHandler():
    print (request.is_json)
    content = request.get_json()
    print (content)
    return 'JSON posted'

if __name__ == "__main__":
    app.run(host='localhost', port=HTTP_PORT )

def actionhandle(request):
    print (request.is_json)
    content = request.get_json()
    print (content)
    return 'JSON posted'
