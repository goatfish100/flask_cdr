import json
import logging
import os
from logging.config import fileConfig
from os.path import dirname, join

import psycopg2
from psycopg2 import pool
import redis
from dotenv import load_dotenv
from flask import Flask, request, g

from handlers import HandlerCdr

application = Flask(__name__)

#load env variables from .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
HTTP_PORT = int(os.getenv('HTTP_PORT'))
DB_CONN = os.getenv('DB_CONN')
ENVIRONMENT = os.getenv('ENVIRONMENT')
REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = os.getenv('REDIS_PORT')
REDIS_DB = os.getenv('REDIS_DB')

# Connect to Postgres and Redis
redis_conn = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
pg_conn = psycopg2.connect(DB_CONN)
pg_conn.autocommit = True
# Set up logger - reading it's ini file
fileConfig('logging_config.ini')
LOGGER = logging.getLogger()
LOGGER.info('starting cdr processing application on port %s', HTTP_PORT)

handle_cdr = HandlerCdr("/tmp")
application = Flask(__name__)

@application.before_request
def before_request():
    g.db = pg_conn
    g.redis = redis_conn
    
@application.route('/data', methods=['POST'])
def inroutine():
    LOGGER.debug("postJsonHandler() called")
    LOGGER.debug(request)
    try:
        content = request.get_json()
    except Exception as e:
        LOGGER.error(e)
        return '{"sucess": false, "message": "invalid json"}'

    LOGGER.debug(content)

    if content['variables']['peer_ip'] is None or \
        content['variables']['destination_number'] is None and \
        (content['variables']['destination_number']  == 'inbound' and \
        content['variables']['direction'] != 'orig'):
        LOGGER.error("Missing fields check for json")
        return '{"failure": "missing fields check"}'

    if (content['channel_data']['direction'] == 'inbound'
            and content['variables']['direction'] != 'orig'):
        LOGGER.debug("inbound direction called")
        return handle_cdr.handle_inbound(content)
    elif (content['channel_data']['direction'] == 'outbound'
          and content['variables']['direction'] != 'orig'):
        LOGGER.debug("outbound direction called")
        return handle_cdr.handle_outbound(content)
    # JL - coping cdr_new.php code -- block did not get called - duplicate
    # elif (content['channel_data']['direction'] == 'inbound' and content['variables']['direction'] == 'orig'):
    #     LOGGER.debug("outbound direction called")
    #     handle_cdr.handle_inbound_orig(content, conn)
    #return '{"success": true}'

@application.route('/test', methods=['GET'])
def heartbeat():
        return '{"success": "true"}'

if __name__ == '__main__':
    #IF set to dev - create dev server for development
    if (ENVIRONMENT == 'dev'):
        LOGGER.info("Environment Dev - using Flask's NON PRODUCTION Server")
        LOGGER.info("DO NOT RUN IN PRODUCTION with this configuration")
        LOGGER.info("http port:" + str(HTTP_PORT))
        application.run(host='localhost', port=HTTP_PORT)
# For Production - launch with
# gunicorn - #gunicorn -w 4 -b localhost:8001 wsgi
