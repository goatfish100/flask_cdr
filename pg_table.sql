-- Table: cdr2.term_b

-- DROP TABLE cdr2.term_b;

CREATE TABLE term_b
(
    uuid uuid,
    aleg_uuid uuid,
    peer_ip inet,
    start_stamp timestamp(0) with time zone,
    start_uepoch bigint,
    progress_stamp timestamp(0) with time zone,
    answer_stamp timestamp(0) with time zone,
    end_stamp timestamp(0) with time zone,
    cdr_date date,
    duration integer NOT NULL,
    progressmsec integer,
    billsec integer,
    hangup_cause character varying COLLATE pg_catalog."default",
    ep_codec_string character varying COLLATE pg_catalog."default",
    dtmf_type character varying COLLATE pg_catalog."default",
    remote_media_ip inet,
    remote_media_port integer,
    proto_specific_hangup_cause character varying(64) COLLATE pg_catalog."default",
    originate_disposition character varying COLLATE pg_catalog."default",
    sip_term_status character varying COLLATE pg_catalog."default",
    sip_term_cause integer,
    hangup_cause_q850 integer,
    digits_dialed character varying COLLATE pg_catalog."default",
    sip_hangup_disposition character varying COLLATE pg_catalog."default",
    rtp_quality numeric(5,2),
    rtp_mos numeric(2,1),
    rtp_audio_out_packet_count integer,
    rtp_audio_in_packet_count integer,
    rate numeric(7,5),
    total numeric(7,5),
    carrier character varying COLLATE pg_catalog."default",
    proxy_ip inet
)
WITH (
    OIDS = FALSE
)
