import json
import logging
import os
import redis
import psycopg2
from handlers import HandlerCdr
from flask import Flask, request 
from logging.config import fileConfig
from os.path import dirname, join
from dotenv import load_dotenv


#load env variables from .env file
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
HTTP_PORT = int(os.getenv('HTTP_PORT'))
DB_CONN = os.getenv('DB_CONN')
ENVIRONMENT = os.getenv('ENVIRONMENT') 
REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = os.getenv('REDIS_PORT')
REDIS_DB = os.getenv('REDIS_DB')

# Connect to Postgres and Redis
redis_conn = redis.Redis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)
conn = psycopg2.connect(DB_CONN)

# Set up logger - reading it's ini file
fileConfig('logging_config.ini')
logger = logging.getLogger()

handle_cdr = HandlerCdr()
app = Flask(__name__)


@app.route('/data', methods = ['POST'])
def index():
    logger.debug("postJsonHandler() called")
    logger.debug(request)
    try:
        content = request.get_json()
    except Exception as e:
        logger.error("Error decoding json from request", e)
        return '{"sucess": false, "message": "invalid json"}'

    logger.debug(content)

    if content['variables']['peer_ip'] is None or \
        content['variables']['destination_number'] is None and \
        (content['variables']['destination_number']  == 'inbound' and \
        content['variables']['direction'] != 'orig'):
        return '{"failure": "missing fields check"}'

    if (content['channel_data']['direction'] == 'inbound' and content['variables']['direction'] != 'orig'): 
        logger.debug("inbound direction called")
        handle_cdr.handle_inbound(content, conn)
    elif (content['channel_data']['direction'] == 'outbound' and content['variables']['direction'] != 'orig'):
        logger.debug("outbound direction called")
        handle_cdr.handle_outbound(content, conn, redis_conn)
    elif (content['channel_data']['direction'] == 'inbound' and content['variables']['direction'] == 'orig'):
        logger.debug("outbound direction called")
        handle_cdr.handle_outbound_orig(content, conn)            
    return '{"success": true}'

